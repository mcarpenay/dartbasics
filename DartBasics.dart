//Dart Refresher, 10/25/21
//-------------------------


// void main() {
//   for (int i = 0; i < 5; i++) {
//     print('hello ${i + 1}');
//   }
// }
 
// Outout:
// hello 1
// hello 2
// hello 3
// hello 4
// hello 5

// void main (){
  
//   //Dart is statically typed
//   //Dart primative Data Types:
  
//   String name = 'Mark';
//   int age = 34;
//   double height = 1.84;
  
//   print("Hello, my name is $name");
//   print("I'm $age years old");
//   print("I'm $height meters tall");
  
//   //String interpolation:
//   //the $ followed by the curly brackets enclosing the combined 
//   //name and length attribute into
//   //a single variable in the first statement below int the 
//   //second statement only the name variable is interprolated
  
//   print("My name has ${name.length} letters");
//   print("My name has $name.length letters");
// }

// // Output:

// // Hello, my name is Mark
// // I'm 34 years old
// // I'm 1.84 meters tall
// // My name has 4 letters
// // My name has Mark.length letters

// //---------------------------------------------


// void main (){
  
//   //Dart is statically typed
//   //Variable types defined at compile time 
//   //(via type inference when using var)
  
//   //  var type is mutable can be changed
//   var name = 'Mark';
//   var age = 34;
//   var height = 1.84;
  
//   print("Hello, my name is $name");
//   print("I'm $age years old");
//   height = 1.9; 
//   print("I'm $height meters tall");
  
//   //String interpolation:
//   //the $ followed by the curly brackets enclosing the combined 
//   //name and length attribute into
//   //a single variable in the first statement below int the 
//   //second statement only the name variable is interprolated
  
//   print("My name has ${name.length} letters");
//   print("My name has $name.length letters");
// }

// //output:
// // Hello, my name is Mark
// // I'm 34 years old
// // I'm 1.9 meters tall
// // My name has 4 letters
// // My name has Mark.length letters


// void main (){
  
//   //Dart is statically typed
//   //Variable types defined at compile time 
//   //(via type inference when using var)
  
//   //  final type is immutable can be changed
//   var name = 'Mark';
//   var age = 34;
//   final height = 1.84;
  
//   print("Hello, my name is $name");
//   print("I'm $age years old");
  
//   print("I'm $height meters tall");
  
//   //String interpolation:
//   //the $ followed by the curly brackets enclosing the combined 
//   //name and length attribute into
//   //a single variable in the first statement below int the 
//   //second statement only the name variable is interprolated
  
//   print("My name has ${name.length} letters");
//   print("My name has $name.length letters");
// }

//output:
// Hello, my name is Mark
// I'm 34 years old
// I'm 1.84 meters tall
// My name has 4 letters
// My name has Mark.length letters

// void main (){
  
//   //Dart is statically typed
//   //Variable types defined at compile time 
//   //(via type inference when using var)
  
//   // dynamic type allow us to change the type 
//   //of the variable throughout the code, used whe we do not know what type 
//   // we want to assign to a variable
  
//   var name = 'Mark';
//   var age = 34;
//   dynamic height = 1.84; 
  
  
//   print("Hello, my name is $name");
//   print("I'm $age years old");
//   height = 'Tall';
//   print("I'm $height meters tall");
  
//   //String interpolation:
//   //the $ followed by the curly brackets enclosing the combined 
//   //name and length attribute into
//   //a single variable in the first statement below int the 
//   //second statement only the name variable is interprolated
  
//   print("My name has ${name.length} letters");
//   print("My name has $name.length letters");
// }

// // output: 
// // Hello, my name is Mark
// // I'm 34 years old
// // I'm Tall meters tall
// // My name has 4 letters
// // My name has Mark.length letters


// void main (){
  
//   //Dart is statically typed
//   //Variable types defined at compile time 
//   //(via type inference when using var)
  
//   // dynamic type allow us to change the type 
//   //of the variable throughout the code, used whe we do not know what type 
//   // we want to assign to a variable
  
//   var name = 'Mark';
//   var age = 34;
//   dynamic height = 1.84; 
  
//   describe(name,age,height);
//   describe('James',10,1.76);
  
// }

// function for creating reusable code
// void describe(String name, int age, double height){
  
//   print("Hello, my name is $name");
//   print("I'm $age years old");
//   print("I'm $height meters tall");
  
// }

// // output: 

// // Hello, my name is Mark
// // I'm 34 years old
// // I'm 1.84 meters tall
// // Hello, my name is James
// // I'm 10 years old
// // I'm 1.76 meters tall

//-------------------------------------------------------------------

/**
void main (){
  
  //Dart is statically typed
  //Variable types defined at compile time 
  //(via type inference when using var)
  
  // dynamic type allow us to change the type 
  //of the variable throughout the code, used whe we do not know what type 
  // we want to assign to a variable
  
  var name = 'Mark';
  var age = 34;
  dynamic height = 1.84; 
  
  final person1 = describe(name,age,height);
  final person2 = describe('James',10,1.76);
  
  print(person1);
  print(person2);
}

//function with a String return type for creating reusable code

String describe(String name, int age, double height){
  return "Hello, my name is $name. I'm $age years old. I'm $height meters tall ";
  
}

// output:
// Hello, my name is Mark. I'm 34 years old. I'm 1.84 meters tall 
// Hello, my name is James. I'm 10 years old. I'm 1.76 meters tall

//-----------------------------------------------------------------------------------
**/

/**

void main (){
  
  //Dart is statically typed
  //Variable types defined at compile time 
  //(via type inference when using var)
  
  // dynamic type allow us to change the type 
  //of the variable throughout the code, used whe we do not know what type 
  // we want to assign to a variable
  
  var name = 'Mark';
  var age = 34;
  dynamic height = 1.84; 
  
  final person1 = describe(name,age); // using the optional argument feature of the function
  final person2 = describe('James',10,1.76);
  
  print(person1);
  print(person2);
}

// creating a function with optional paramater that can be null
//or set to a default value as in the emample below
//using the [] brackets around the paramater makes it
//optional

String describe(String name, int age, [double height = 0.0]){
  return "Hello, my name is $name. I'm $age years old. I'm $height meters tall ";
  
}

// output:
// Hello, my name is Mark. I'm 34 years old. I'm 0 meters tall 
// Hello, my name is James. I'm 10 years old. I'm 1.76 meters tall 

**/

void main (){
  
//   //Dart is statically typed
//   //Variable types defined at compile time 
//   //(via type inference when using var)
  
//   // dynamic type allow us to change the type 
//   //of the variable throughout the code, used whe we do not know what type 
//   // we want to assign to a variable
  
//   var name = 'Mark';
//   var age = 34;
//   dynamic height = 1.84; 
  
//   final person1 = describe(name:name, age:age, height: height); // using the optional argument feature of the function
//   final person2 = describe(name:'James', age:10, height:1.76);
  
//   print(person1);
//   print(person2);
// }

// // creating a function with {}
// // -positional
// // -named 
// //paramaters that are more verbose but clearer to read 
// //cannot have both optional and positional named paramaters in a function

// String describe({String? name, int? age, double height = 0.0}){
//   return "Hello, my name is $name. I'm $age years old. I'm $height meters tall ";
  
// }

// // output:

// // Hello, my name is Mark. I'm 34 years old. I'm 1.84 meters tall 
// // Hello, my name is James. I'm 10 years old. I'm 1.76 meters tall 


// void main (){
  
//   //Dart is statically typed
//   //Variable types defined at compile time 
//   //(via type inference when using var)
  
//   // dynamic type allow us to change the type 
//   //of the variable throughout the code, used whe we do not know what type 
//   // we want to assign to a variable
  
//   var name = 'Mark';
//   var age = 34;
//   dynamic height = 1.84; 
  
//   final person1 = describe(name:name, age:age, height: height); // using the optional argument feature of the function
//   final person2 = describe(name:'James', age:10, height:1.76);
  
//   sayName('Mark');
//   print(person1);
//   print(person2);
// }

// // creating a function with the arrow operator (this is syntactic sugar no special purpose)
// // => it replaces the open and closed curly brackets and the return key word in the function definition 
// // see describe1() which is the same as describe()
// //used for functions with just one line of code in the body

// String describe({String? name, int? age, double height = 0.0}){
//   return "Hello, my name is $name. I'm $age years old. I'm $height meters tall ";  
// }

// String describe1({String? name, int? age, double height = 0.0}) =>
//   "Hello, my name is $name. I'm $age years old. I'm $height meters tall ";  

// void sayName(String name) => print("Hello, I'm $name");

// output: 
// Hello, I'm Mark
// Hello, my name is Mark. I'm 34 years old. I'm 1.84 meters tall 
// Hello, my name is James. I'm 10 years old. I'm 1.76 meters tall 



